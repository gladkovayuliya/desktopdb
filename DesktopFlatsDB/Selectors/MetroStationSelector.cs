﻿using DesktopFlatsDB.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace DesktopFlatsDB.Selectors
{
	public class MetroStationSelector: DataTemplateSelector
	{
		public DataTemplate FakeMetroStationTemplate { get; set; }

		public DataTemplate RealMetroStationTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			if (container is FrameworkElement element && item is MetroStationViewModel station)
			{
				if (station.IsFake)
				{
					return FakeMetroStationTemplate;
				}

				return RealMetroStationTemplate;
			}
			return null;
		}
	}
}

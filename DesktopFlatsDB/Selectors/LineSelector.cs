﻿using DesktopFlatsDB.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace DesktopFlatsDB.Selectors
{
	public class LineSelector : DataTemplateSelector
	{
		public DataTemplate FakeLineTemplate { get; set; }

		public DataTemplate RealLineTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			if (container is FrameworkElement element && item is LineViewModel line)
			{
				if (line.IsFake)
				{
					return FakeLineTemplate;
				}

				return RealLineTemplate;
			}
			return null;
		}
	}
}

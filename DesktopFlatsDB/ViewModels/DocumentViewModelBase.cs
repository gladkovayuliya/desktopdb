﻿using DesktopFlatsDB.Abstract;
using DesktopFlatsDB.MVVM;
using System;

namespace DesktopFlatsDB.ViewModels
{
	public class ExecutionResult<T>
	{
		public bool Success { get; set; }
		public T Result { get; set; }
	}

	public class DocumentViewModelBase: ViewModelBase
	{

		public string Error
		{
			get => _error;
			set => this.UpdateValue(value, ref _error);
		}

		public DocumentViewModelBase(IDBConnector connector)
		{
			_connector = connector;
		}

		protected bool Execute(Action action)
		{
			try
			{
				action.Invoke();
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		protected ExecutionResult<T> Execute<T>(Func<T> function)
		{
			try
			{
				var res = function.Invoke();
				return new ExecutionResult<T>
				{
					Success = true,
					Result = res
				};
			}
			catch (Exception)
			{
				return new ExecutionResult<T>
				{
					Success = false,
					Result = default(T)
				};
			}
		}

		protected IDBConnector _connector;
		private string _error;
	}
}

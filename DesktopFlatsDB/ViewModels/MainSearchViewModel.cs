﻿using DesktopFlatsDB.Abstract;
using DesktopFlatsDB.Models;
using DesktopFlatsDB.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using DesktopFlatsDB.Utility.Extensions;
using DesktopFlatsDB.HelperClasses;
using DesktopFlatsDB.Services;

namespace DesktopFlatsDB.ViewModels
{
	public class MainSearchViewModel: DocumentViewModelBase
	{
#region Commands

		public ICommand ShowFlatsCommand { get; private set; }

		public ICommand ClearLineCommand { get; private set; }

		public ICommand ClearStationListCommand { get; private set; }

		public ICommand ToggleFavoriteCommand { get; private set; }
#endregion

		public string FavoritesError
		{
			get => _favoritesError;
			set => this.UpdateValue(value, ref _favoritesError);
		}

		public List<MetroStationViewModel> StationList { get; }

		public ObservableCollection<AdvertismentViewModel> AdvertismentList { get; set; }

		public AdvertismentViewModel CurrentAdvertisment
		{
			get => _currentAdvertisment;
			set
			{
				this.UpdateValue(value, ref _currentAdvertisment);
				this.AddAdvertismentdToWatched();
			}
		}

		public List<LineViewModel> Lines { get; }

		public LineViewModel CurrentLine
		{
			get => _currentLine;
			set
			{
				this.UpdateValue(value, ref _currentLine);
				if (value != null && !value.IsFake)
				{
					this.SelectStationsByLine();
				}
			}
		}

		public FilterViewModel Filter { get; }

		public MainSearchViewModel(IDBConnector connector, List<MetroStationViewModel> stationList)
			:base(connector)
		{
			StationList = new List<MetroStationViewModel>();
			var fake = new MetroStationViewModel(new MetroStation { Title = "Выберите станцию" }, new LineViewModel(new Line()));
			StationList.Add(fake);
			StationList.AddRange(stationList);

			Filter = new FilterViewModel();

			AdvertismentList = new ObservableCollection<AdvertismentViewModel>();

			Lines = _connector.GetLines()
				.Select(line => new LineViewModel(line)).ToList();
			var fakeLine = new LineViewModel(new Line {Title = "Выберите линию" });
			Lines.Insert(0, fakeLine);
			CurrentLine = fakeLine;

			this.CreateCommands();
		}

		private void CreateCommands()
		{
			ShowFlatsCommand = CommandFactory.Create(this.ShowFlatsExecute, this.ShowFlatsCanExecute);
			ClearLineCommand = CommandFactory.Create(this.ClearLineExecute);
			ClearStationListCommand = CommandFactory.Create(this.ClearStationListExecute);
			ToggleFavoriteCommand = CommandFactory.Create(this.ToggleFavoriteExecute);
		}

		private void ShowFlatsExecute()
		{
			Filter.Model.StationsId = StationList.Where(s => s.IsChecked)
				.Select(s => s.Station.MetroStationId);
			var result = this.Execute(() => _connector.GetAdvertismentByFilter(Filter.Model));
			if (!result.Success)
			{
				Error = "Неизвестная ошибка";
				return;
			}
			var advStructs = result.Result;
			var list = this.CreateAdvertismentsViewModels(advStructs);
			AdvertismentList.Clear();
			AdvertismentList.AddRange(list);
			if (list.Count == 0)
			{
				Error = "По вашему запросу ничего не найдено";
			}
			else
			{
				Error = String.Empty;
			}

		}

		private List<AdvertismentViewModel> CreateAdvertismentsViewModels(IEnumerable<AdvertismentStruct> advStructs)
		{
			return advStructs.Select(a => AdvertismentFactory.Create(a, StationList)).ToList();
		}

		private void SelectStationsByLine()
		{
			foreach(var station in StationList)
			{
				station.IsChecked = station.Line.Model.LineId == CurrentLine.Model.LineId;
			}
		}

		private void ClearLineExecute()
		{
			CurrentLine = Lines[0];
		}

		private void ClearStationListExecute()
		{
			CurrentLine = Lines[0];
			foreach (var station in StationList)
			{
				station.IsChecked = false;
			}
		}

		private void ToggleFavoriteExecute()
		{
			var advId = CurrentAdvertisment.Model.AdvertismentId;
			var userId = UserContext.CurrentUser.UserId;
			if (CurrentAdvertisment.ContainsInFavorites)
			{
				var success  = this.Execute(() => _connector.DeleteFavorite(userId, advId));
				if(!success)
				{
					FavoritesError = "Ошибка при удалении закладки";
					return;
				}

				UserContext.FavoritesAdvertisments.Remove(advId);
			}
			else
			{

				var success  = this.Execute( () =>_connector.CreateFavorite(userId, advId));
				if (!success)
				{
					FavoritesError = "Ошибка при добавлении закладки";
					return;
				}
				UserContext.FavoritesAdvertisments.Add(advId);
			}

				Error = String.Empty;
				CurrentAdvertisment.UpdateFavoriteState();
		}

		private void AddAdvertismentdToWatched()
		{
			if (CurrentAdvertisment == null)
			{
				return;
			}
			var adId = CurrentAdvertisment.Model.AdvertismentId;
			if (UserContext.WatchedAdvertisments.Contains(adId))
			{
				return;
			}
			UserContext.WatchedAdvertisments.Add(adId);
			_connector.UpdateViews(adId);
				
		}


		private bool ShowFlatsCanExecute() => Filter.IsValid;

		private LineViewModel _currentLine;

		private AdvertismentViewModel _currentAdvertisment;

		private string _favoritesError;
	}
}

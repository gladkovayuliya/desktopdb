﻿using DesktopFlatsDB.MVVM;
using DesktopFlatsDB.Models;

namespace DesktopFlatsDB.ViewModels
{
	public class FilterViewModel : ValidatableViewModelBase
	{

		public Filter Model =>
			(Filter)this.ValidationModel;

		public string RoomsCountString
		{
			get => Model.RoomsCountString;
			set
			{
				Model.RoomsCountString = value;
				this.OnPropertyChanged();
			}
		}

		public string District
		{
			get => Model.District;
			set
			{
				Model.District = value;
				this.OnPropertyChanged();
			}
		}

		public string Street
		{
			get => Model.Street;
			set
			{
				Model.Street = value;
				this.OnPropertyChanged();
			}
		}

		public int? MinimumRent
		{
			get => Model.MinimumRent;
			set
			{
				Model.MinimumRent = value;
				this.OnPropertyChanged();
			}
		}

		public int? MaximumRent
		{
			get => Model.MaximumRent;
			set
			{
				Model.MaximumRent = value;
				this.OnPropertyChanged();
			}
		}

		public int? MetroStationDistance
		{
			get => Model.MetroStationDistance;
			set
			{
				Model.MetroStationDistance = value;
				this.OnPropertyChanged();
			}
		}

		public FilterViewModel()
			: this (new Filter())
		{

		}

		protected FilterViewModel(Filter model) 
			: base(model)
		{
		}
	}
}

﻿using DesktopFlatsDB.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;

namespace DesktopFlatsDB.ViewModels
{
	public class ImageGaleryViewModel:ViewModelBase
	{
		public ObservableCollection<BitmapImage> Images { get;}

		public BitmapImage Current
		{
			get => _current;
			set => this.UpdateValue(value, ref _current);
		}

		public ImageGaleryViewModel(IEnumerable<BitmapImage> images)
		{
			if (images == null)
			{
				throw new ArgumentNullException(nameof(images));
			}

			Images = new ObservableCollection<BitmapImage>(images);
		}

		private BitmapImage _current;
	}
}

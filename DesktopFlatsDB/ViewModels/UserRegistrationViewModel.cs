﻿using DesktopFlatsDB.Models;

namespace DesktopFlatsDB.ViewModels
{
	public class UserRegistrationViewModel: UserViewModel
	{
		public new UserRegistrationInfo Model =>
			(UserRegistrationInfo)ValidationModel;

		public string Password
		{
			get => Model.Password;
			set
			{
				Model.Password = value;
				OnPropertyChanged();
			}
		}

		public string RepeatPassword
		{
			get => Model.RepeatPassword;
			set
			{
				Model.RepeatPassword = value;
				OnPropertyChanged();
			}
		}

		public UserRegistrationViewModel()
			: base (new UserRegistrationInfo())
		{

		}
	}
}

﻿using DesktopFlatsDB.Abstract;
using DesktopFlatsDB.HelperClasses;
using DesktopFlatsDB.MVVM;
using System;
using System.Windows.Input;

namespace DesktopFlatsDB.ViewModels
{
	public class SingleAdvertismentWatchingViewModel: DocumentViewModelBase
	{
		public ICommand ToggleFavoriteCommand { get; private set; }

		public AdvertismentViewModel Current { get; }

		public string FavoritesError
		{
			get => _favoritesError;
			set => this.UpdateValue(value, ref _favoritesError);
		}

		public SingleAdvertismentWatchingViewModel(IDBConnector connector, AdvertismentViewModel advertisment)
			:base(connector)
		{
			Current = advertisment ??
				throw new ArgumentNullException(nameof(advertisment));

			ToggleFavoriteCommand = CommandFactory.Create(this.ToggleFavoriteExecute);
		}

		private void ToggleFavoriteExecute()
		{
			var advId = Current.Model.AdvertismentId;
			var userId = UserContext.CurrentUser.UserId;
			if (Current.ContainsInFavorites)
			{
				var success = this.Execute(() => _connector.DeleteFavorite(userId, advId));
				if (!success)
				{
					FavoritesError = "Ошибка при удалении закладки";
					return;
				}

				UserContext.FavoritesAdvertisments.Remove(advId);
			}
			else
			{
				var success = this.Execute(() => _connector.CreateFavorite(userId, advId));
				if (!success)
				{
					FavoritesError = "Ошибка при добавлении закладки";
					return;
				}
				UserContext.FavoritesAdvertisments.Add(advId);
			}

			Error = String.Empty;

		}

		private string _favoritesError;
	}
}

﻿using System;
using DesktopFlatsDB.MVVM;
using DesktopFlatsDB.Models;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using DesktopFlatsDB.HelperClasses;

namespace DesktopFlatsDB.ViewModels
{
	public class AdvertismentViewModel : ValidatableViewModelBase
	{
		public Advertisment Model =>
			(Advertisment)ValidationModel;

		public bool IsValidComplex =>
			IsValid && Flat.IsValidComplex;

		public int RealtorId
		{
			get => Model.RealtorId;
			set => Model.RealtorId = value;
		}

		public DateTime PlacementDate
		{
			get => Model.PlacementDate;
			set => Model.PlacementDate = value;
		}


		public int ViewsCount
		{
			get => Model.ViewsCount;
			set => Model.ViewsCount = value;
		}

		public decimal Rent
		{
			get => Model.Rent;
			set
			{
				Model.Rent = value;
				this.OnPropertyChanged();
			}
		}

		public string Comment
		{
			get => Model.Comment;
			set
			{
				Model.Comment = value;
				this.OnPropertyChanged();
			}
		}

		public AccomodationListViewModel AccomodationList { get; set; }

		public bool HasAccomodations => 
			AccomodationList!= null && AccomodationList.Accomodations.Count > 0;
	

		public bool IsFavoriteButtonVisible =>
			!UserContext.IsGuest &&
			UserContext.CurrentUser.UserId != RealtorId;

		public bool ContainsInFavorites =>
			!UserContext.IsGuest &&
			UserContext.FavoritesAdvertisments.Contains(Model.AdvertismentId);
		

		public ObservableCollection<BitmapSource> Images { get; set; }

		public FlatViewModel Flat { get; set; }

		public User Realtor { get; set; }

		public AdvertismentViewModel()
			: this (new Advertisment())
		{

		}

		public AdvertismentViewModel(Advertisment model) :
			base(model)
		{
			Flat = new FlatViewModel();
		}

		public void UpdateFavoriteState()
		{
			this.OnPropertyChanged(nameof(ContainsInFavorites));
		}
	}
}

﻿using DesktopFlatsDB.MVVM;
using DesktopFlatsDB.Models;

namespace DesktopFlatsDB.ViewModels
{
	public class AdressViewModel : ValidatableViewModelBase
	{

		public Adress Model =>
			(Adress)ValidationModel;

		public string District
		{
			get => Model.District;
			set
			{
				Model.District = value;
				this.OnPropertyChanged();
			}
		}

		public string Street
		{
			get => Model.Street;
			set
			{
				Model.Street = value;
				this.OnPropertyChanged();
			}
		}

		public string HouseNumber
		{
			get => Model.HouseNumber;
			set
			{
				Model.HouseNumber = value;
				this.OnPropertyChanged();
			}
		}

		public int? BlockNumber
		{
			get => Model.HouseBlockNumber;
			set
			{
				Model.HouseBlockNumber = value;
				this.OnPropertyChanged();
			}
		}

		public string FullAdress =>
			this.GetFullAdress(District, Street, HouseNumber, BlockNumber);

		public MetroStationViewModel MetroStation { get; set; }

		public AdressViewModel()
			: this ( new Adress())
		{

		}

		public AdressViewModel(Adress model) :
			base(model)
		{
		}

		private string GetFullAdress(string district, string street, string houseNumber, int? blockNumber)
		{
			var adress = $"Район {district}, улица {street}, д{houseNumber}";
			if (blockNumber!=null)
			{
				adress += $", ст {blockNumber}";
			}

			return adress;
		}


	}
}

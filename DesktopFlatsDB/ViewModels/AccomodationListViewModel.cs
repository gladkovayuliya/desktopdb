﻿using System;
using System.Collections.Generic;
using System.Linq;
using DesktopFlatsDB.MVVM;

namespace DesktopFlatsDB.ViewModels
{
	public class AccomodationListViewModel: ViewModelBase
	{
		public List<AccomodationViewModel> Accomodations { get; }

		public int RowCount { get; }

		public int ColumnCount => _columnCount;

		public AccomodationListViewModel(IEnumerable<AccomodationViewModel> accomodations)
		{
			if (accomodations==null)
			{
				throw new ArgumentNullException(nameof(accomodations));
			}

			Accomodations = accomodations.ToList();

			RowCount = Accomodations.Count == _columnCount
				? 1
				: Accomodations.Count / _columnCount + 1;
		}


		private const int _columnCount = 2;
	}
}

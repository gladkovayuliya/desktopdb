﻿using DesktopFlatsDB.MVVM;
using DesktopFlatsDB.Models;

namespace DesktopFlatsDB.ViewModels
{
	public class AccomodationViewModel : ValidatableViewModelBase
	{
		public Accomodation Model =>
			(Accomodation)ValidationModel;

		public bool IsChecked { get; set; }

		public string Title => Model.Title;

		public AccomodationViewModel()
			: this(new Accomodation())
		{

		}

		public AccomodationViewModel(Accomodation model) 
			: base(model)
		{
		}
	}
}

﻿using DesktopFlatsDB.Abstract;
using DesktopFlatsDB.HelperClasses;
using DesktopFlatsDB.MVVM;
using DesktopFlatsDB.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace DesktopFlatsDB.ViewModels
{
	public class ProfileViewModel: DocumentViewModelBase
	{
		public event EventHandler<AdvertismentEventArgs> FavoriteAdvertismentSelected;
		public event EventHandler<AdvertismentEventArgs> CreatedAdvertismentSelected;

		#region Commands

		public ICommand SaveUserCommand { get; private set; }

		public ICommand OpenFavoriteCommand { get; private set; }

		public ICommand DeleteFavoriteCommand { get; private set; }

		public ICommand OpenCreatedCommand { get; private set; }

		public ICommand DeleteCreatedCommand { get; private set; }

		#endregion

		public UserViewModel User { get; }

		public ObservableCollection<AdvertismentViewModel> Favorites { get; }

		public AdvertismentViewModel CurrentFavorite
		{
			get => _currentFavorite;
			set => this.UpdateValue(value, ref _currentFavorite);
		}

		public AdvertismentViewModel CurrentCreated
		{
			get => _currentCreated;
			set => this.UpdateValue(value, ref _currentCreated);
		}

		public bool HasFavorites => 
			Favorites != null && Favorites.Count > 0;

		public bool HasCreated =>
			CreatedAdvertisments != null && CreatedAdvertisments.Count > 0;

		public List<AdvertismentViewModel> CreatedAdvertisments { get; }

		public bool IsRealtor => 
			User.Role == Models.RoleType.Realtor;

		public ProfileViewModel(IDBConnector connector, List<MetroStationViewModel> stationList)
			:base(connector)
		{

			_stationList = stationList;

			User = new UserViewModel(UserContext.CurrentUser);

			var favorites = this.GetFavorites();
			Favorites = new ObservableCollection<AdvertismentViewModel>(favorites);

			if (IsRealtor)
			{
				CreatedAdvertisments = this.GetCreated();
			}

			this.CreateCommands();
		}

		private void CreateCommands()
		{
			SaveUserCommand = CommandFactory.Create(this.SaveUserExecute, this.SaveUserCanExecute);
			OpenFavoriteCommand = CommandFactory.Create(this.OpenFavoriteExecute, this.FavoriteCanExecute);
			DeleteFavoriteCommand = CommandFactory.Create(this.DeleteFavoriteExecute, this.FavoriteCanExecute);
			OpenCreatedCommand = CommandFactory.Create(this.OpenCreatedExecute, this.CreatedCanExecute);
			DeleteCreatedCommand = CommandFactory.Create(this.DeleteCreatedExecute, this.CreatedCanExecute);
		}

		private List<AdvertismentViewModel> GetFavorites()
		{
			var res = this.Execute(()  => _connector.GetFavorites(User.Model.UserId));
			if (!res.Success)
			{
				Error = "Произошла неизвестная ошибка";
				return null;
			}
			var advStructs = res.Result;
			if (advStructs == null)
			{
				return null;
			}
			return advStructs.Select(a => AdvertismentFactory.Create(a, _stationList)).ToList();
		}

		private List<AdvertismentViewModel> GetCreated()
		{
			var advStructs = _connector.GetCreatedAdvertisments(User.Model.UserId);
			return advStructs.Select(a => AdvertismentFactory.Create(a, _stationList)).ToList();
		}

		#region Execute

		private void SaveUserExecute()
		{
			_connector.UpdateUser(User.Model);
		}

		private void OpenFavoriteExecute()
		{
			FavoriteAdvertismentSelected?.Invoke(this, new AdvertismentEventArgs(CurrentFavorite));
		}

		private void DeleteFavoriteExecute()
		{
			var advId = CurrentFavorite.Model.AdvertismentId;
			var userId = User.Model.UserId;
			UserContext.FavoritesAdvertisments.Remove(advId);
			var success  = this.Execute(() => _connector.DeleteFavorite(userId, advId));
			if (!success)
			{
				Error = "Ошибка при удалении";
				return;
			}

			Error = String.Empty;
			Favorites.Remove(CurrentFavorite);
			if (Favorites.Count == 0)
			{
				this.OnPropertyChanged(nameof(HasFavorites));
				CurrentFavorite = null;
			}
			else
			{
				CurrentFavorite = Favorites[0];
			}
		}

		private void OpenCreatedExecute()
		{
			CreatedAdvertismentSelected?.Invoke(this, new AdvertismentEventArgs(CurrentCreated));
		}

		private void DeleteCreatedExecute()
		{
			var success = this.Execute( () =>_connector.DeleteAdvertisment(CurrentCreated.Flat.Adress.Model.AdressId));
			if (!success)
			{
				Error = "Ошибка при удалении";
				return;
			}

			Error = String.Empty;
			CreatedAdvertisments.Remove(CurrentCreated);
			if (CreatedAdvertisments.Count == 0)
			{
				this.OnPropertyChanged(nameof(HasCreated));
				CurrentCreated = null;
			}
			else
			{
				CurrentCreated = CreatedAdvertisments[0];
			}
		}

		#endregion

		#region CanExecute
		private bool SaveUserCanExecute() => User.IsValid;

		private bool CreatedCanExecute() => CurrentCreated != null;

		private bool FavoriteCanExecute() => CurrentFavorite != null;
		#endregion

		private readonly List<MetroStationViewModel> _stationList;
		private AdvertismentViewModel _currentFavorite;
		private AdvertismentViewModel _currentCreated;
	}
}

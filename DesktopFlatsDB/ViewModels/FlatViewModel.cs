﻿using DesktopFlatsDB.MVVM;
using DesktopFlatsDB.Models;

namespace DesktopFlatsDB.ViewModels
{
	public class FlatViewModel : ValidatableViewModelBase
	{
		public Flat Model =>
			(Flat)ValidationModel;

		public bool IsValidComplex =>
			this.IsValid && Adress.IsValid;

		public int  MetroStationDistance
		{
			get => Model.MetroStationDistance;
			set
			{
				Model.MetroStationDistance = value;
				this.OnPropertyChanged();
			}
		}

		public int RoomsCount
		{
			get => Model.RoomsCount;
			set
			{
				Model.RoomsCount = value;
				this.OnPropertyChanged();
			}
		}
		
		public int FloorNumber
		{
			get => Model.FloorNumber;
			set
			{
				Model.FloorNumber = value;
				this.OnPropertyChanged();

			}
		}

		public float GeneralArea
		{
			get => Model.GeneralArea;
			set
			{
				Model.GeneralArea = value;
				this.OnPropertyChanged();
			}
		}

		public float KitchenArea
		{
			get => Model.KitchenArea;
			set
			{
				Model.KitchenArea = value;
				this.OnPropertyChanged();
			}
		}

		public bool Elevator
		{
			get => Model.Elevator;
			set
			{
				Model.Elevator = value;
				this.OnPropertyChanged();
			}
		}

		public AdressViewModel Adress { get; set; }

		public FlatViewModel()
			: this (new Flat())
		{

		}

		public FlatViewModel(Flat model) 
			: base(model)
		{
			Adress = new AdressViewModel();
		}
	}
}

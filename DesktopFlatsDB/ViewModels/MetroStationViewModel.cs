﻿using DesktopFlatsDB.Models;
using DesktopFlatsDB.MVVM;
using System;

namespace DesktopFlatsDB.ViewModels
{
	public class MetroStationViewModel: ViewModelBase
	{
		public MetroStation Station { get; }

		public LineViewModel Line { get; }

		public bool IsChecked
		{
			get => _isChecked;
			set => this.UpdateValue(value, ref _isChecked);
		}

		public bool IsFake => Station.MetroStationId == 0;

		public MetroStationViewModel(MetroStation station, LineViewModel line)
		{
			Station = station ??
				throw new ArgumentNullException(nameof(station));
			Line = line ??
				throw new ArgumentNullException(nameof(line));

		}

		private bool _isChecked;
	}
}

﻿using System;
using DesktopFlatsDB.MVVM;
using System.Windows.Input;
using System.Windows;
using DesktopFlatsDB.HelperClasses;
using DesktopFlatsDB.Abstract;
using DesktopFlatsDB.Models;
using System.Collections.Generic;
using System.Linq;
using DesktopFlatsDB.Views;

namespace DesktopFlatsDB.ViewModels
{
	public class AuthorizeViewModel: DocumentViewModelBase
	{
		public string Login { get; set; }

		public string Password { get; set; }

		public ICommand EnterCommand { get; private set; }

		public ICommand EnterAsGuestCommand { get; private set; }

		public ICommand RegistrationCommand { get; private set; }

		public AuthorizeViewModel(Window mainWindow, ViewModelBase registrtationViewModel, IDBConnector connector)
			: base(connector)
		{
			_mainWnd = mainWindow ??
				throw new ArgumentNullException(nameof(mainWindow));

			_regVM = registrtationViewModel ??
				throw new ArgumentNullException(nameof(registrtationViewModel));

			_connector = connector ??
				throw new ArgumentNullException(nameof(connector));

#if DEBUG
			Login = "super-realtor";
			Password = "qwerty12345";
#endif

			this.CreateCommands();
		}

		private void CreateCommands()
		{
			EnterCommand = CommandFactory.Create<Window>(this.EnterExecute, _ => EnterCanExecute());
			EnterAsGuestCommand = CommandFactory.Create<Window>(this.EnterAsGuestExecute, _ => true);
			RegistrationCommand = CommandFactory.Create(this.RegistrationExecute);
		}

		#region Execute

		private void EnterExecute(Window window)
		{
			var executionResult = this.Execute(() => _connector.AuthorizeUser(Login, Password));
			if (!executionResult.Success)
			{
				Error = "Неизвестная ошибка";
				return;
			}

			var user = executionResult.Result;
			if (user == null)
			{
				Error = "Неверный логин или пароль";
				return;
			}
			this.OpenMainWindow(window, user, isGuest: false);

		}

		private void EnterAsGuestExecute(Window window) =>
			this.OpenMainWindow(window, null, isGuest: true);

		private void RegistrationExecute()
		{
			var wnd = new RegistrationWindow
			{
				DataContext = _regVM
			};
			wnd.ShowDialog();
		}

		#endregion

		#region CanExecute 

		private bool EnterCanExecute() =>
			!String.IsNullOrWhiteSpace(Login) &&
			!String.IsNullOrWhiteSpace(Password);
		
#endregion

		private void OpenMainWindow(Window current, User user, bool isGuest)
		{
			UserContext.CurrentUser = user;
			UserContext.IsGuest = isGuest;
			UserContext.WatchedAdvertisments = new List<int>();
			if (!isGuest)
			{
				var executionResult = this.Execute(() => _connector.GetFavoritesIds(user.UserId));
				if (!executionResult.Success)
				{
					Error = "Неизвестная ошибка";
					return;
				}

				var res = executionResult.Result;
				if (res == null)
				{
					Error = "Неизвестная ошибка";
					return;
				}

				UserContext.FavoritesAdvertisments = res.ToList();
			}
			current.Close();
			_mainWnd.Show();

		}

		private bool CheckUser()
		{
			return true;
		}

		private Window _mainWnd;
		private ViewModelBase _regVM;
	}
}

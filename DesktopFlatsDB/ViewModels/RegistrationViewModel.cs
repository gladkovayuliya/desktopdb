﻿using System;
using DesktopFlatsDB.MVVM;
using System.Windows.Input;
using DesktopFlatsDB.Abstract;
using System.Windows;

namespace DesktopFlatsDB.ViewModels
{
	public class RegistrationViewModel: DocumentViewModelBase
	{
		public ICommand RegistrationCommand { get; private set; }

		public UserRegistrationViewModel User { get; }

		public bool IsRealtor
		{
			get
			{
				return User.Role == Models.RoleType.RegularUser
					? false
					: true;
			}
			set
			{
				User.Role = value
					? Models.RoleType.Realtor
					: Models.RoleType.RegularUser;
			}
		}
		

		public RegistrationViewModel(IDBConnector connector)
			:base(connector)
		{
			User = new UserRegistrationViewModel();
			RegistrationCommand = CommandFactory.Create<Window>(this.RegistrationExecute, _=> this.RegistrationCanExecute());
		}

		private  void RegistrationExecute(Window current)
		{
			var success =  this.Execute( () =>_connector.RegisterUser(User.Model));
			if (success)
			{
				current.Close();
				return;
			}
			Error = "Ошибка регистрации";
		}

		private bool RegistrationCanExecute() =>
			User.IsValid && String.Equals(User.Password, User.RepeatPassword);

	}
}

﻿using DesktopFlatsDB.Abstract;
using DesktopFlatsDB.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using DesktopFlatsDB.Utility.Extensions;
using DesktopFlatsDB.HelperClasses;

namespace DesktopFlatsDB.ViewModels
{
	public class AdvertismentCreationViewModel: DocumentViewModelBase
	{
		public event EventHandler AdrvertismentCreated; 

		public ICommand LoadPhotosCommand { get; private set; }

		public ICommand CreateCommand { get; private set; }

		public List<MetroStationViewModel> StationList { get; }

		public AccomodationListViewModel AccomodationList { get; }

		public AdvertismentViewModel Advertisment { get; set; }

		public ObservableCollection<BitmapSource> Images { get; }

		public bool IsCreating { get; set; }

		public string ButtonTitle => IsCreating
			? "Создать" 
			: "Изменить";


		public AdvertismentCreationViewModel(IDBConnector connector, IImagesProvider imgProvider, List<MetroStationViewModel> stationList, AdvertismentViewModel created)
			: this (connector, imgProvider, stationList)
		{
			Advertisment = created;
			foreach (var a in AccomodationList.Accomodations)
			{
				var contains = Advertisment.AccomodationList.Accomodations
					.Where(ac => ac.Model.AccomodationId == a.Model.AccomodationId)
					.Count() > 0;
				if (contains)
				{
					a.IsChecked = true;
				}
			}
			Images = Advertisment.Images;

			IsCreating = false;
		}

		public AdvertismentCreationViewModel(IDBConnector connector, IImagesProvider imgProvider, List<MetroStationViewModel> stationList)
			:base(connector)
		{
			_imgProvider = imgProvider;
			StationList = stationList;
			Advertisment = new AdvertismentViewModel
			{
				RealtorId = UserContext.CurrentUser.UserId,
				PlacementDate = DateTime.Now,
				ViewsCount = 0
			};
			
			var accomodations = _connector.GetAccomodationsList().Select(a => new AccomodationViewModel(a));
			AccomodationList = new AccomodationListViewModel(accomodations);

			Images = new ObservableCollection<BitmapSource>();

			IsCreating = true;

			this.CreateCommands();
		}



		private void CreateCommands()
		{
			LoadPhotosCommand = CommandFactory.Create(this.LoadPhotosExecute);
			CreateCommand = CommandFactory.Create(this.CreateExecute, this.CreateCanExecute);
		}

		private void LoadPhotosExecute()
		{
			var res = _imgProvider.GetImages(_maxImageSize, _maxImagesCount);
			if (res.error == ImageProviderErrorType.NoErrors)
			{
				Error = String.Empty;
				Images.Clear();
				Images.AddRange(res.images);
				return;
			}
			var error = res.error.GetDescription();
			if (res.error == ImageProviderErrorType.ImageNotFound || 
				res.error == ImageProviderErrorType.WrongImageWeight)
			{
				error += $" {res.wrongImage}";
			}
			Error = error;
		}

		private void CreateExecute()
		{
			var param = new AdvertismentStruct
			{
				Advertisment = Advertisment.Model,
				Flat = Advertisment.Flat.Model,
				Adress = Advertisment.Flat.Adress.Model,
				MetroStation = Advertisment.Flat.Adress.MetroStation.Station
			};

			var accomodations = AccomodationList.Accomodations.Where(a => a.IsChecked)
				.Select(a => a.Model);

			if (IsCreating)
			{
				var success = this.Execute(() => _connector.PlaceAdvertisment(param, accomodations, Images));
				if (!success)
				{
					Error = "Ошибка при создании объявления";
					return;
				}
			}
			else
			{
				var success = this.Execute(() => _connector.UpdateAdvertisment(param, accomodations, Images));
				if (!success)
				{
					Error = "Ошибка при редактировании объявления";
					return;
				}
			}
			
			Error = String.Empty;
			AdrvertismentCreated?.Invoke(this, new EventArgs());
		}

		private bool CreateCanExecute() =>
			Advertisment.IsValidComplex;
		
		private IImagesProvider _imgProvider;
		private const int _maxImageSize = 3072;
		private const int _maxImagesCount = 10;
	}
}

﻿using DesktopFlatsDB.Models;
using DesktopFlatsDB.MVVM;
using System;
using System.Windows.Media;

namespace DesktopFlatsDB.ViewModels
{
	public class LineViewModel : ViewModelBase
	{
		public Line Model { get; }

		public SolidColorBrush LineColor { get; }

		public string Title =>
			Model.Title;

		public bool IsFake =>
			Model.LineId == 0;

		public LineViewModel(Line model)
		{
			Model = model ??
				throw new ArgumentNullException(nameof(model));

			LineColor = new SolidColorBrush(Color.FromRgb((byte)Model.ColorR,(byte) Model.ColorG, (byte)Model.ColorB));
		}
	}
}

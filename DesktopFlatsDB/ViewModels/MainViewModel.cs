﻿using System;
using System.Collections.Generic;
using System.Linq;
using DesktopFlatsDB.MVVM;
using DesktopFlatsDB.Abstract;
using DesktopFlatsDB.HelperClasses;
using System.Windows.Input;
using System.Windows;
using System.Collections.ObjectModel;

namespace DesktopFlatsDB.ViewModels
{
	public class MainViewModel: ViewModelBase
	{
		public UserViewModel CurrentUser { get; set; }

		public ObservableCollection<MenuButton> PermissibleButtons { get; private set; }

		public ViewModelBase Content
		{
			get => _content;
			private set
			{
				_content = value;
				OnPropertyChanged();
			}
		}
		
		public List<MetroStationViewModel> StationList { get; }

		public MainViewModel(IDBConnector connector, IImagesProvider imgProvider)
		{
			_connector = connector ??
				throw new ArgumentNullException(nameof(connector));

			_imgProvider = imgProvider ??
				throw new ArgumentNullException(nameof(imgProvider));

			PermissibleButtons = new ObservableCollection<MenuButton>();

			StationList = _connector.GetMetroStationsWithLines().ToList();

			_search = new MainSearchViewModel(_connector, StationList);

			Content = _search;
		}

		public void WindowLoaded(object sender, RoutedEventArgs e)
		{
			if (!UserContext.IsGuest)
			{
				CurrentUser = new UserViewModel(UserContext.CurrentUser);
			}

			this.CreateCommands();
			this.CreateButtonList();
		}

		private void CreateButtonList()
		{

			var aboutButton = new MenuButton
			{
				Title = "О программе",
				Command = this._aboutCommand,
			};

			var searchButton = new MenuButton
			{
				Title = "Поиск",
				Command = this._searchCommand,
			};
			
			PermissibleButtons.Add(searchButton);

			if (CurrentUser != null)
			{
				if (CurrentUser.Role == Models.RoleType.Realtor)
				{
					var advertismentButton = new MenuButton
					{
						Title = "Создать объявление",
						Command = this._advertismentCommand,
					};

					PermissibleButtons.Add(advertismentButton);
				}

				var profileButton = new MenuButton
				{
					Title = "Личный кабинет",
					Command = this._profileCommand,
				};
				
				PermissibleButtons.Add(profileButton);
			}
		}

		private void CreateCommands()
		{
			_aboutCommand = CommandFactory.Create(this.AboutExecute);
			_searchCommand = CommandFactory.Create(this.SearchExecute);
			_advertismentCommand = CommandFactory.Create(this.AdvertismentExecute);
			_profileCommand = CommandFactory.Create(this.ProfileExecute);
			_exitCommand = CommandFactory.Create(this.ExitExecute);
		}

		#region Execute

		private void SearchExecute()
		{
			Content = _search;
		}

		private void AboutExecute()
		{
			Content = new AboutViewModel();
		}

		private void AdvertismentExecute()
		{
			var advertismentCreationViewModel = new AdvertismentCreationViewModel(_connector, _imgProvider, StationList);
			advertismentCreationViewModel.AdrvertismentCreated += this.OnAdrvertismentCreated;
			Content = advertismentCreationViewModel;
			
		}

		private void OnAdrvertismentCreated(object sender, EventArgs e)
		{
			Content = _search;
		}

		private void ProfileExecute()
		{
			var profile = new ProfileViewModel(_connector, StationList);
			profile.FavoriteAdvertismentSelected += this.OnFavoriteAdvertismentSelected;
			profile.CreatedAdvertismentSelected += this.OnCreatedAdvertismentSelected;
			Content = profile;
		}

		private void OnCreatedAdvertismentSelected(object sender, AdvertismentEventArgs e)
		{
			var edition = new AdvertismentCreationViewModel(_connector, _imgProvider, StationList, e.Advertisment);
			edition.AdrvertismentCreated += this.OnAdrvertismentCreated;
			Content = edition;
		}

		private void OnFavoriteAdvertismentSelected(object sender, AdvertismentEventArgs e)
		{
			Content = new SingleAdvertismentWatchingViewModel(_connector, e.Advertisment);
		}

		private void ExitExecute()
		{

		}

		#endregion


		#region CanExecute
#endregion
		private readonly IDBConnector _connector;
		private readonly IImagesProvider _imgProvider;
		private ICommand _searchCommand;
		private ICommand _aboutCommand;
		private ICommand _advertismentCommand;
		private ICommand _profileCommand;
		private ICommand _exitCommand;
		private ViewModelBase _content;
		private MainSearchViewModel _search;
	}
}

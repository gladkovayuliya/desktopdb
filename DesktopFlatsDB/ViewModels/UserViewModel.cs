﻿using DesktopFlatsDB.MVVM;
using DesktopFlatsDB.Models;

namespace DesktopFlatsDB.ViewModels
{
	public class UserViewModel : ValidatableViewModelBase
	{
		public User Model =>
			(User)ValidationModel;

		public string Login
		{
			get => Model.Login;
			set
			{
				Model.Login = value;
				this.OnPropertyChanged();
			}
		}

		public string FirstName
		{
			get => Model.FirstName;
			set
			{
				Model.FirstName = value;
				this.OnPropertyChanged();
			}
		}

		public string LastName
		{
			get => Model.LastName;
			set
			{
				Model.LastName = value;
				this.OnPropertyChanged();
			}
		}

		public string Email
		{
			get => Model.Email;
			set
			{
				Model.Email = value;
				this.OnPropertyChanged();
			}
		}

		public string PhoneNumber
		{
			get => Model.PhoneNumber;
			set
			{
				Model.PhoneNumber = value;
				this.OnPropertyChanged();
			}
		}
		
		public RoleType Role
		{
			get => (RoleType)Model.RoleId;
			set
			{
				Model.RoleId = (int)value;
				this.OnPropertyChanged();
			}
		}

		public UserViewModel():
			this(new User())
		{

		}

		public UserViewModel(User model) :
			base(model)
		{
		}
	}
}

﻿
namespace DesktopFlatsDB.Models
{
	public class MetroStation
	{
		public int MetroStationId { get; set; }

		public string Title { get; set; }

		public int LineId { get; set; }
	}
}

﻿using System.ComponentModel.DataAnnotations;

namespace DesktopFlatsDB.Models
{
	public class Flat
	{
		public int FlatId { get; set; }
		
		public int AdressId { get; set; }

		[Required(ErrorMessage = "Время до метро не указано")]
		[Range(0, 60, ErrorMessage = "Время до метро не может быть отрицательным и не должно превышать час")]
		public int MetroStationDistance { get; set; }

		[Required(ErrorMessage = "Количество комнат не указано")]
		[Range(1, 15, ErrorMessage = "Количество комнат не может быть отрицательным и превышать 15")]
		public int RoomsCount { get; set; }

		[Required(ErrorMessage = "Этаж не указан")]
		[Range(1, 100, ErrorMessage = "Этаж не может быть отрицательным и превышать 100")]
		public int FloorNumber { get; set; }

		[Required(ErrorMessage = "Общая площадь не указана")]
		[Range(1, 1000, ErrorMessage = "Общая площадь не может быть отрицательной и превышать 1000")]
		public float GeneralArea { get; set; }

		[Required(ErrorMessage = "Площадь кухни не указана")]
		[Range(1, 1000, ErrorMessage = "Площадь  кухни не может быть отрицательной и превышать 1000")]
		public float KitchenArea { get; set; }

		public bool Elevator { get; set; }

	}
}

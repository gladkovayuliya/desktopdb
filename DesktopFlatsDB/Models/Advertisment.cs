﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DesktopFlatsDB.Models
{
	public class Advertisment
	{
		public int AdvertismentId { get; set; }

		public int RealtorId { get; set; }

		public int FlatId { get; set; }

		public int ViewsCount { get; set; }

		public DateTime PlacementDate { get; set; }

		[Required(ErrorMessage = "Ежемесячная оплата не указана")]
		[Range(0, Int32.MaxValue,ErrorMessage = "Оплата не должна быть меньше 0" )]
		public Decimal Rent { get; set; }

		public string Comment { get; set; }
	}

}

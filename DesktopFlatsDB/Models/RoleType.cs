﻿
namespace DesktopFlatsDB.Models
{
	public enum RoleType
	{
		RegularUser = 1,
		Realtor = 2
	}
}

﻿
namespace DesktopFlatsDB.Models
{
	public class Line
	{
		public int LineId { get; set; }

		public string Title { get; set; }

		public int ColorR { get; set; }

		public int ColorG { get; set; }

		public int ColorB { get; set; }

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using DesktopFlatsDB.Utility.Extensions;
using System.ComponentModel.DataAnnotations;
using DesktopFlatsDB.Utility.Attributes;

namespace DesktopFlatsDB.Models
{
	public class Filter
	{
		[QueryGenerating("MetroStationId", ComparisonSymbol.Equal)]
		public IEnumerable<int> StationsId { get; set; }

		[RegularExpression("(([1-9]|1[0-5])(,|$) *)+",
			ErrorMessage = "Количество комнат не может быть больше 15")]
		public string RoomsCountString
		{
			get => _roomsCount;
			set
			{
				_roomsCount = value;
				RoomsCount = this.Parse(_roomsCount);
			}
		}

		[QueryGenerating("RoomsCount", ComparisonSymbol.Equal)]
		public IEnumerable<int> RoomsCount { get; set; }

		[QueryGenerating("District", ComparisonSymbol.Equal)]
		public string District { get; set; }

		[QueryGenerating("Street", ComparisonSymbol.Equal)]
		public string Street { get; set; }

		[QueryGenerating("Rent", ComparisonSymbol.BiggerOrEqual)]
		[Range(0, Int32.MaxValue, ErrorMessage = "Плата не может быть отрицательной")]
		public int? MinimumRent { get; set; }

		[QueryGenerating("Rent", ComparisonSymbol.Lower)]
		[Range(0, Int32.MaxValue, ErrorMessage = "Плата не может быть отрицательной")]
		public int? MaximumRent { get; set; }

		[QueryGenerating("MetroStationDistance", ComparisonSymbol.LowerOrEqual)]
		[Range(0, 60, ErrorMessage = "Время до метро не может быть отрицательным и не должно превышать час")]
		public int? MetroStationDistance { get; set; }

		private IEnumerable<int> Parse(string roomsCountString)
		{
			return roomsCountString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
				.Select(s => s.ToInt32())
				.Distinct();
		}

		private string _roomsCount;
	}
}

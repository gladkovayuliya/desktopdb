﻿using System.ComponentModel.DataAnnotations;

namespace DesktopFlatsDB.Models
{
	public class User
	{
		public int UserId { get; set; }

		[Required(ErrorMessage = "Не указано имя")]
		[StringLength(64, ErrorMessage = "Максимальная длина 64 символа")]
		public string FirstName { get; set; }

		[Required(ErrorMessage = "Не указана фамилия")]
		[StringLength(64, ErrorMessage = "Максимальная длина 64 символа")]
		public string LastName { get; set; }

		[Required(ErrorMessage = "Не указан логин")]
		[StringLength(32, MinimumLength = 6,  ErrorMessage = "Максимальная длина 32 символа, минимальная - 6")]
		[RegularExpression("^[a-zA-Z1-9-+@_]+",
			ErrorMessage = "Допустимый набор символов для логина: a-zA-Z1-9-+@_")]
		public string Login { get; set; }

		[Required(ErrorMessage = "Не указана почта")]
		[StringLength(64, ErrorMessage = "Максимальная длина 64 символа")]
		[EmailAddress(ErrorMessage = "Неверный формат электронной почты")]
		public string Email { get; set; }

		[Required(ErrorMessage = "Не указан номер телефона")]
		[StringLength(64, ErrorMessage = "Максимальная длина 64 символа")]
		[Phone(ErrorMessage = "Неверный формат номера телефона")]
		public string PhoneNumber { get; set; }

		public int RoleId { get; set; }
		
		public byte[] PasswordHash { get; set; }

		public byte[] Salt { get; set; }

		public User()
		{
			RoleId = (int)RoleType.RegularUser;
		}

	}
}

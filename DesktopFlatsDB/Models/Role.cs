﻿
namespace DesktopFlatsDB.Models
{
	public class Role
	{
		public int RoleId { get; set; }
		public string Title { get; set; }
	}
}

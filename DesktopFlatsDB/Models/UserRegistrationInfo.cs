﻿using System.ComponentModel.DataAnnotations;

namespace DesktopFlatsDB.Models
{
	public class UserRegistrationInfo: User
	{
		[Required(AllowEmptyStrings = false, ErrorMessage = "Не указан пароль")]
		[StringLength(maximumLength:255, MinimumLength = 8, ErrorMessage = "Длина пароля не должна быть больше 255 символов и меньше 8")]
		public string Password { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Повторите пароль")]
		[StringLength(maximumLength: 255,MinimumLength = 8, ErrorMessage = "Длина пароля не должна быть больше 255 символов и меньше 8")]
		public string RepeatPassword { get; set; }
	}
}

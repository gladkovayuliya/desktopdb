﻿
namespace DesktopFlatsDB.Models
{
	public class FlatsImages
	{
		public int Id { get; set; }

		public int FlatId { get; set; }

		public byte[] Image { get; set; }
	}

}

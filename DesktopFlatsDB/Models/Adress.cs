﻿using System.ComponentModel.DataAnnotations;

namespace DesktopFlatsDB.Models
{
	public class Adress
	{
		public int AdressId { get; set; }
		
		public int MetroStationId { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Район не указан")]
		[MaxLength(64, ErrorMessage = "Максимальная длина 64 символа")]
		public string District { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Улица не указана")]
		[MaxLength(64, ErrorMessage = "Максимальная длина 64 символа")]
		public string Street { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Номер дома не указан")]
		[MaxLength(32, ErrorMessage = "Максимальная длина 32 символа")]
		[RegularExpression(@"(([1-9][0-9]?)(\/)?)+",
			ErrorMessage = "Допустимый набор символов для номера дома: 1-9 /")]
		public string HouseNumber { get; set; }

		[Range(1, 20)]
		[RegularExpression("^[1-9/]+",
			ErrorMessage = "Допустимый набор символов для строения: 1-9 /")]
		public int? HouseBlockNumber { get; set; }
	}
}

﻿using System.ComponentModel;

namespace DesktopFlatsDB.Abstract
{
	public enum DBErrorType
	{
		[Description("")]
		NoErrors,

		[Description("Пользователь с таким логином уже существует")]
		LoginExisted,

		[Description("Пользователь с таким адреоом элекронной почты уже существует")]
		EmailExisted,

		[Description("Неверный логин или пароль")]
		WrongLoginOrPassword,

		[Description("Неизвестная ошибка")]
		UnknownError
	}
}

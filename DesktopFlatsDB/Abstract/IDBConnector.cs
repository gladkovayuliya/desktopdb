﻿using DesktopFlatsDB.Models;
using DesktopFlatsDB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace DesktopFlatsDB.Abstract
{
	public struct AdvertismentStruct
	{
		public Advertisment Advertisment { get; set; }
		public Flat Flat { get; set; }
		public Adress Adress { get; set; }
		public MetroStation MetroStation { get; set; }
		public User Realtor { get; set; }
		public IEnumerable<BitmapSource> Images { get; set; }
		public IEnumerable<Accomodation> AccomodationList { get; set; }
	}


	public interface IDBConnector
	{
		void RegisterUser(UserRegistrationInfo user);
		User AuthorizeUser(string login, string password);
		IEnumerable<MetroStationViewModel> GetMetroStationsWithLines();
		IEnumerable<Accomodation> GetAccomodationsList();
		void PlaceAdvertisment(AdvertismentStruct advertismentStruct, IEnumerable<Accomodation> accomodations, 
			IEnumerable<BitmapSource> images);
		IEnumerable<AdvertismentStruct> GetAdvertismentByFilter(Filter filter);
		IEnumerable<Line> GetLines();
		void UpdateViews(int advertismentId);
		IEnumerable<AdvertismentStruct> GetFavorites(int userId);
		IEnumerable<int> GetFavoritesIds(int userId);
		void CreateFavorite(int userId, int advertismentId);
		void DeleteFavorite(int userId, int advertismentId);
		IEnumerable<AdvertismentStruct> GetCreatedAdvertisments(int realtorId);
		void UpdateUser(User user);
		void UpdateAdvertisment(AdvertismentStruct advertismentStruct, IEnumerable<Accomodation> accomodations,
			IEnumerable<BitmapSource> images);
		void DeleteAdvertisment(int adressId);
	}
}

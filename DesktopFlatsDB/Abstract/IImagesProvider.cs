﻿using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace DesktopFlatsDB.Abstract
{
	public interface IImagesProvider
	{
		(IEnumerable<BitmapSource> images, ImageProviderErrorType error, string wrongImage) GetImages(int maxWeight, int maxCount);
	}
}

﻿using System.ComponentModel;

namespace DesktopFlatsDB.Abstract
{
	public enum ImageProviderErrorType
	{
		[Description("Изображение не найдено")]
		ImageNotFound,

		[Description("Слишком большой размер изображения")]
		WrongImageWeight,

		[Description("Изображения не выбраны")]
		NoImages,

		[Description("Неизвестная ошибка")]
		UnknownError,

		NoErrors
	}
}

﻿using DesktopFlatsDB.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DesktopFlatsDB.Models;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using System.Security.Cryptography;
using DesktopFlatsDB.ViewModels;
using System.Windows.Media.Imaging;
using System.IO;
using DesktopFlatsDB.Utility.Attributes;
using DesktopFlatsDB.Utility.Extensions;
using System.Collections;

namespace DesktopFlatsDB.Services
{

	internal struct AdvertismentFlat
	{
		public int AdvertismentId { get; set; }
		public int FlatId { get; set; }
	}

	public class DBConnector : IDBConnector
	{
		public DBConnector(string connectionTitle)
		{
			if (String.IsNullOrWhiteSpace(connectionTitle))
			{
				throw new ArgumentException(nameof(connectionTitle));
			}
			_connectionString = ConfigurationManager.ConnectionStrings[connectionTitle].ConnectionString;
		}

		public void RegisterUser(UserRegistrationInfo user)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				this.CalculateHashnSalt(user);
				connection.QueryAsync("dbo.CreateUser", new { user.FirstName, user.LastName, user.Login, user.Email, user.PhoneNumber, user.RoleId, user.PasswordHash, user.Salt },
					commandType: CommandType.StoredProcedure);
			}
		}
		
		public User AuthorizeUser(string login, string password)
		{
			var user = this.GetUserByLogin(login);
			if (user == null)
			{
				return null;
			}
			var success = this.ComparePasswordsHash(user, password);
			return success
				? user
				: null;
		}

		public IEnumerable<MetroStationViewModel> GetMetroStationsWithLines()
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				var lines =  connection.Query<Line>("dbo.GetLines", commandType: CommandType.StoredProcedure);
				var stations = connection.Query<MetroStation>("dbo.GetStations", commandType: CommandType.StoredProcedure);
				var linesViewModels = lines.Select(line => new LineViewModel(line)).ToArray();
				return stations.Select(station => new MetroStationViewModel(station, linesViewModels[station.LineId - 1]));

			}
		}

		public IEnumerable<Accomodation> GetAccomodationsList()
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				return connection.Query<Accomodation>("dbo.GetAccomodations", commandType: CommandType.StoredProcedure);
			}
		}

		public void PlaceAdvertisment(AdvertismentStruct advertismentStruct, IEnumerable<Accomodation> accomodations,
			IEnumerable<BitmapSource> images)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				var af  = connection.QueryFirstOrDefault<AdvertismentFlat>("dbo.PlaceAdvertisment", this.CreateParameterObjectForAdvertisment(advertismentStruct), commandType: CommandType.StoredProcedure);
				foreach (var accomodation in accomodations)
				{
					connection.Query("dbo.CreateFlatAccomodation", new { af.FlatId, accomodation.AccomodationId }, commandType: CommandType.StoredProcedure);
				}
				foreach (var image in images)
				{
					var imgBytes = this.FromImageToBinary(image);
					connection.Query("dbo.CreateFlatImage", new { af.FlatId, imgBytes }, commandType: CommandType.StoredProcedure);
				}
			}
		}

		public IEnumerable<AdvertismentStruct> GetAdvertismentByFilter(Filter filter)
		{
			var query = this.CreateQueryByFilter(filter);
			var result = new List<AdvertismentStruct>();
			using (var connection = new SqlConnection(_connectionString))
			{
				var advertismentIds = connection.Query<int>(query);
				foreach (var a in advertismentIds)
				{
					var advertisment = connection.QueryFirstOrDefault<Advertisment>("dbo.GetAdvertismentById", new { AdvertismentId = a }, commandType: CommandType.StoredProcedure);
					var advStruct = this.GetFullAdvertisment(advertisment);
					result.Add(advStruct);
				}
				return result;
			}
		}

		public IEnumerable<Line> GetLines()
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				return connection.Query<Line>("dbo.GetLines", commandType: CommandType.StoredProcedure);

			}
		}

		public void UpdateViews(int advertismentId)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Query("dbo.UpdateViews", new { advertismentId }, commandType: CommandType.StoredProcedure);
			}
			
		}

		public IEnumerable<AdvertismentStruct> GetFavorites(int userId)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				var res = new List<AdvertismentStruct>();
				var advertisments = connection.Query<Advertisment>("dbo.GetFavoritesAdvertisments", new { userId }, commandType: CommandType.StoredProcedure);
				foreach (var a in advertisments)
				{
					var advStruct = this.GetFullAdvertisment(a);
					res.Add(advStruct);
				}
				return res;
			}
			
		}

		public IEnumerable<int> GetFavoritesIds(int userId)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				var res =  connection.Query<int>("dbo.GetFavoritesAdvertismentsIds", new { userId }, commandType: CommandType.StoredProcedure);
				return res;
			}
		}
		
		public void CreateFavorite(int userId, int advertismentId)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Query("dbo.CreateFavoriteAdvertisment", new { userId, advertismentId }, commandType: CommandType.StoredProcedure);
			}
		}

		public void DeleteFavorite(int userId, int advertismentId)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				
				connection.Query("dbo.DeleteFavoriteAdvertisment", new { userId, advertismentId }, commandType: CommandType.StoredProcedure);
			}
		}

		public IEnumerable<AdvertismentStruct> GetCreatedAdvertisments(int realtorId)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				var res = new List<AdvertismentStruct>();
				var advertisments = connection.Query<Advertisment>("dbo.GetCreatedAdvertisments", new { realtorId }, commandType: CommandType.StoredProcedure);
				foreach (var a in advertisments)
				{
					var advStruct = this.GetFullAdvertisment(a);
					res.Add(advStruct);
				}
				return res;
			}
		}

		public void UpdateUser(User user)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Query("dbo.UpdateUser", new { user.UserId, user.Email, user.PhoneNumber }, commandType: CommandType.StoredProcedure);
			}
		}

		public void UpdateAdvertisment(AdvertismentStruct advertismentStruct, IEnumerable<Accomodation> accomodations, IEnumerable<BitmapSource> images)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				var advertismentId = advertismentStruct.Advertisment.AdvertismentId;
				var rent = advertismentStruct.Advertisment.Rent;
				var comment = advertismentStruct.Advertisment.Comment;
				var flatId = advertismentStruct.Flat.FlatId;

				connection.Query("dbo.UpdateAdvertisment", new { advertismentId, rent, comment }, commandType: CommandType.StoredProcedure);
				connection.Query("dbo.DeleteAccomodations", new { flatId }, commandType: CommandType.StoredProcedure);
				connection.Query("dbo.DeleteImages", new { flatId }, commandType: CommandType.StoredProcedure);
				foreach (var accomodation in accomodations)
				{
					connection.Query("dbo.CreateFlatAccomodation", new { flatId, accomodation.AccomodationId }, commandType: CommandType.StoredProcedure);
				}
				foreach (var image in images)
				{
					var imgBytes = this.FromImageToBinary(image);
					connection.Query("dbo.CreateFlatImage", new { flatId, imgBytes }, commandType: CommandType.StoredProcedure);
				}
			}
		}
		
		public void DeleteAdvertisment(int adressId)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Query("dbo.DeleteAdress", new { adressId }, commandType: CommandType.StoredProcedure);
			}
		}

		private AdvertismentStruct GetFullAdvertisment(Advertisment advertisment)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				var flat = connection.QueryFirstOrDefault<Flat>("dbo.GetFlatById", new { FlatId = advertisment.FlatId }, commandType: CommandType.StoredProcedure);
				var adress = connection.QueryFirstOrDefault<Adress>("dbo.GetAdressById", new { AdressId = flat.AdressId }, commandType: CommandType.StoredProcedure);
				var realtor = connection.QueryFirstOrDefault<User>("dbo.GetUserById", new { UserId = advertisment.RealtorId }, commandType: CommandType.StoredProcedure);
				var imageBytes = connection.Query<byte[]>("dbo.GetImages", new { FlatId = advertisment.FlatId }, commandType: CommandType.StoredProcedure);
				var accomodationList = connection.Query<Accomodation>("dbo.GetAccomodationsByFlatId", new { FlatId = advertisment.FlatId }, commandType: CommandType.StoredProcedure);
				var advStruct = new AdvertismentStruct
				{
					Advertisment = advertisment,
					Flat = flat,
					Adress = adress,
					Realtor = realtor,
					AccomodationList = accomodationList
				};
				if (imageBytes != null)
				{
					var images = imageBytes.Select(bytes => this.FromBinaryToImage(bytes)).ToList();
					advStruct.Images = images;
				}
				return advStruct;
			}

		}

		private User GetUserByLogin(string userLogin)
		{
			using (var connection = new SqlConnection(_connectionString))
			{
				return connection.Query<User>("dbo.GetUserByLogin", new { Login = userLogin }, commandType: CommandType.StoredProcedure).FirstOrDefault();
			}
		}

		private void CalculateHashnSalt(UserRegistrationInfo user)
		{
			var salt= new byte[32];
			using (var cryptoProvider = new RNGCryptoServiceProvider())
			{
				cryptoProvider.GetNonZeroBytes(salt);
			}

			var passwordBytes = Encoding.ASCII.GetBytes(user.Password);
			user.Salt = salt;
			user.PasswordHash = this.CalculateHash(passwordBytes, salt);
		}

		private bool ComparePasswordsHash(User user, string password)
		{
			var passwordBytes = Encoding.ASCII.GetBytes(password);
			var hash = this.CalculateHash(passwordBytes, user.Salt);
			var strUserHash = Convert.ToBase64String(user.PasswordHash);
			var strHash = Convert.ToBase64String(hash);
			return String.Equals(strHash, strUserHash);
		}

		private byte[] CalculateHash(byte[] password, byte[] salt)
		{
			var withSalt = password.Concat(salt).ToArray();
			using (var sha = new SHA256Managed())
			{
				return sha.ComputeHash(withSalt);
			}

		}

		private byte[] FromImageToBinary(BitmapSource image)
		{
			var encoder = new JpegBitmapEncoder();
			encoder.QualityLevel = 100;
			using (MemoryStream stream = new MemoryStream())
			{
				encoder.Frames.Add(BitmapFrame.Create(image));
				encoder.Save(stream);
				var bytes = stream.ToArray();
				stream.Close();
				return bytes;
			}
		}

		private BitmapSource FromBinaryToImage(byte[] bytes)
		{
			var stream = new MemoryStream(bytes);
			return BitmapFrame.Create(stream);

		}

		private object CreateParameterObjectForAdvertisment(AdvertismentStruct advertismentStruct)
		{
			var adress = advertismentStruct.Adress;
			var flat = advertismentStruct.Flat;
			var metroStation = advertismentStruct.MetroStation;
			var advertisment = advertismentStruct.Advertisment;
			return new
			{
				metroStation.MetroStationId,
				adress.District,
				adress.Street,
				adress.HouseNumber,
				adress.HouseBlockNumber,
				flat.MetroStationDistance,
				flat.RoomsCount,
				flat.FloorNumber,
				flat.GeneralArea,
				flat.KitchenArea,
				flat.Elevator,
				advertisment.RealtorId,
				advertisment.ViewsCount,
				advertisment.PlacementDate,
				advertisment.Rent,
				advertisment.Comment
			};
		}

		private string CreateQueryByFilter(Filter filter)
		{
			var res = new StringBuilder("SELECT AdvertismentId FROM dbo.FilterInformation");
			var where = new List<string>();
			var properties = filter.GetType().GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
			var ap = "\'";
			foreach (var property in properties)
			{
				var query = new StringBuilder();
				var attribute = (QueryGeneratingAttribute)property.GetCustomAttributes(typeof(QueryGeneratingAttribute), false).FirstOrDefault();
				if (attribute == default(QueryGeneratingAttribute))
				{
					continue;
				}
				var sign = attribute.ComparisonSymbol.GetDescription();
				var columnName = attribute.ColumnName;
				if (property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)) && property.PropertyType!=typeof(string))
				{
					query.Append("(");
					var list = new List<string>();
					var enumerable = (IEnumerable)property.GetValue(filter, null);
					if (enumerable != null)
					{
						foreach (var item in enumerable)
						{
							if (item.GetType() != typeof(string))
							{
								list.Add($"{columnName} {sign} {item.ToString()}");
							}
							else
							{
								list.Add($"{columnName} {sign} {ap}{item.ToString()}{ap}");
							}
						}
						if (list.Count > 0)
						{
							query.Append(String.Join(" OR ", list));
							query.Append(")");
							where.Add(query.ToString());
						}
					}
				}
				else
				{
					var propertyValue = property.GetValue(filter);
					if (propertyValue != null)
					{
						if (propertyValue.GetType() != typeof(string))
						{
							query.Append($"{attribute.ColumnName} {sign} {property.GetValue(filter)}");
						}
						else
						{
							query.Append($"{attribute.ColumnName} {sign} {ap}{property.GetValue(filter)}{ap}");
						}
						where.Add(query.ToString());
					}
				}
			}
			if (where.Count > 0)
			{
				res.Append(" WHERE ");
				res.Append(String.Join(" AND ", where));
			}
			res.Append(";");
			return res.ToString();
		}


		private readonly string _connectionString;
	}
}

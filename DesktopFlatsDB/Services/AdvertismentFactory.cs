﻿using DesktopFlatsDB.Abstract;
using DesktopFlatsDB.Utility.Extensions;
using DesktopFlatsDB.ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media.Imaging;

namespace DesktopFlatsDB.Services
{
	public static class AdvertismentFactory
	{
		public static AdvertismentViewModel Create(AdvertismentStruct advStruct, IEnumerable<MetroStationViewModel> stations)
		{
				var metroStation = stations.Where(s => s.Station.MetroStationId == advStruct.Adress.MetroStationId).SingleOrDefault();
				var adress = new AdressViewModel(advStruct.Adress)
				{
					MetroStation = metroStation
				};

				var flat = new FlatViewModel(advStruct.Flat)
				{
					Adress = adress
				};

				var advertisment = new AdvertismentViewModel(advStruct.Advertisment)
				{
					Flat = flat,
					Realtor = advStruct.Realtor,
				};

				if (advStruct.AccomodationList != null)
				{
					var accomodations = advStruct.AccomodationList.Select(a => new AccomodationViewModel(a));
					var accomodationList = new AccomodationListViewModel(accomodations);
					advertisment.AccomodationList = accomodationList;
				}

				advertisment.Images = new ObservableCollection<BitmapSource>();

				if (advStruct.Images != null)
				{
					advertisment.Images.AddRange(advStruct.Images);
				}

				return advertisment;
		}
	}
}

﻿using DesktopFlatsDB.Abstract;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;

namespace DesktopFlatsDB.Services
{
	public class ImagesProvider : IImagesProvider
	{
		public (IEnumerable<BitmapSource> images, ImageProviderErrorType error, string wrongImage)
			GetImages(int maxWeight, int maxCount)
		{
			var paths = this.GetImagesPaths().ToArray();

			if (paths.Length == 0)
			{
				return (null, ImageProviderErrorType.NoImages, String.Empty);
			}

			var count = paths.Length  < maxCount
				? paths.Length
				: maxCount;

			var result = new BitmapSource[count];

			for (var i = 0; i < count; ++i)
			{
				var path = paths[i];
				try
				{
					var fileInfo = new FileInfo(path);
					var length = fileInfo.Length / 1024;
					if (length > maxWeight)
					{
						return (null, ImageProviderErrorType.WrongImageWeight, Path.GetFileName(path));
					}
					var image = this.CreateImage(path);
					result[i] = image;
				}
				catch (ArgumentException _)
				{
					return (null, ImageProviderErrorType.ImageNotFound, Path.GetFileName(path));
				}
				catch (Exception _)
				{
					return (null, ImageProviderErrorType.UnknownError, Path.GetFileName(path));
				}
			}
			return (result, ImageProviderErrorType.NoErrors, String.Empty);
		}

		private IEnumerable<string> GetImagesPaths()
		{
			var dialog = new OpenFileDialog
			{
				Multiselect = true,
				Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
							"JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
							"Portable Network Graphic (*.png)|*.png",

			};

			return dialog.ShowDialog() == true
				? dialog.FileNames
				: Enumerable.Empty<string>();

		}

		private BitmapSource CreateImage(string path)
		{
			var uri = new Uri(Path.Combine("file://", path));
			return new BitmapImage(uri);
		}
	}
}

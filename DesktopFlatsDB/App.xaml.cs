﻿using DesktopFlatsDB.Services;
using DesktopFlatsDB.ViewModels;
using DesktopFlatsDB.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace DesktopFlatsDB
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			var connector = new DBConnector("DefaultConnection");

			var imgProvider = new ImagesProvider();

			var mainDataContext = new MainViewModel(connector, imgProvider);
			var mainWnd = new MainWindow
			{
				DataContext = mainDataContext
			};

			mainWnd.Loaded += mainDataContext.WindowLoaded;

			var regDataContext = new RegistrationViewModel(connector);

			var authDataContext = new AuthorizeViewModel(mainWnd, regDataContext, connector);
			var authWnd = new AuthorizeWindow
			{
				DataContext = authDataContext
			};
			authWnd.ShowDialog();
		}

	
	}
}

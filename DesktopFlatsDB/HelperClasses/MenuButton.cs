﻿using System.Windows.Input;

namespace DesktopFlatsDB.HelperClasses
{
	public class MenuButton
	{
		public string Title { get; set; }
		public ICommand Command { get; set; }
		public int Column { get; set; }
	}
}

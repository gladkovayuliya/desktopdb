﻿using DesktopFlatsDB.ViewModels;
using System;

namespace DesktopFlatsDB.HelperClasses
{
	public class AdvertismentEventArgs: EventArgs
	{
		public AdvertismentViewModel Advertisment { get; set; }

		public AdvertismentEventArgs(AdvertismentViewModel advertisment)
		{
			Advertisment = advertisment;
		}
	}
}

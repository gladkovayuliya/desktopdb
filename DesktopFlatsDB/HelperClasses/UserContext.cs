﻿using DesktopFlatsDB.Models;
using System.Collections.Generic;

namespace DesktopFlatsDB.HelperClasses
{
	public static class UserContext
	{
		public static User CurrentUser { get; set; }
		public static bool IsGuest { get; set; }
		public static List<int> WatchedAdvertisments{ get; set; }
		public static List<int> FavoritesAdvertisments { get; set; }
	}
}

﻿using System;
using System.ComponentModel;

namespace DesktopFlatsDB.Utility.Attributes
{
	public enum ComparisonSymbol
	{
		[Description("=")]
		Equal,

		[Description(">=")]
		BiggerOrEqual,

		[Description("<")]
		Lower,

		[Description("<=")]
		LowerOrEqual
	}

	public class QueryGeneratingAttribute: Attribute
	{
		public string ColumnName { get;}

		public ComparisonSymbol ComparisonSymbol { get;}

		public QueryGeneratingAttribute(string columnName, ComparisonSymbol comparisonSymbol)
		{
			ColumnName = columnName;
			ComparisonSymbol = comparisonSymbol;
		}
	}
}

﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace DesktopFlatsDB.Utility.Extensions
{
	public static class StringExtensions
	{
		public static string FromSecureToString(this SecureString secure)
		{
			var ptr = IntPtr.Zero;
			try
			{
				ptr = Marshal.SecureStringToGlobalAllocUnicode(secure);
				return Marshal.PtrToStringUni(ptr);
			}
			finally
			{
				if (ptr != IntPtr.Zero)
				{
					Marshal.ZeroFreeGlobalAllocUnicode(ptr);
				}
			}
		}

		public static int ToInt32(this string str)
		{
			return Int32.Parse(str);
		}

	}
}

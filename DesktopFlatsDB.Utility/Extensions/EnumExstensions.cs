﻿using System;
using System.ComponentModel;

namespace DesktopFlatsDB.Utility.Extensions
{
	public static class EnumExstensions
	{
		public static string GetDescription(this Enum enumValue)
		{
			var enumType = enumValue.GetType();
			var field = enumType.GetField(enumValue.ToString());
			var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), inherit: false);

			return attributes.Length == 0
				? enumValue.ToString()
				: ((DescriptionAttribute)attributes[0]).Description;
		}
		
		
	}
}

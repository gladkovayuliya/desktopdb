﻿using System;
using System.Windows.Input;

namespace DesktopFlatsDB.MVVM
{
	public static class CommandFactory
	{
		public static ICommand Create(Action action)
		{
			if (action == null)
			{
				throw new ArgumentNullException(nameof(action));
			}

			return new RelayCommand<object>(_ =>  action());
		}

		public static ICommand Create<T>(Action<T> action)
		{
			if (action == null)
			{
				throw new ArgumentNullException(nameof(action));
			}

			return new RelayCommand<T>(action);
		}

		public static ICommand Create(Action<object> action) =>
			 new RelayCommand<object>(action);

		public static ICommand Create (Action action, Func<bool> validate)
		{
			if (action == null)
			{
				throw new ArgumentNullException(nameof(action));
			}

			if (validate == null)
			{
				throw new ArgumentNullException(nameof(validate));
			}

			return new RelayCommand<object>(_ => action(), _ => validate());
		}

		public static ICommand Create(Action<object> action, Func<object,bool> validate)
		{
			if (action == null)
			{
				throw new ArgumentNullException(nameof(action));
			}

			if (validate == null)
			{
				throw new ArgumentNullException(nameof(validate));
			}

			return new RelayCommand<object>(action, validate);
		}

		public static ICommand Create<T>(Action<T> action, Func<T, bool> validate)
		{
			if (action == null)
			{
				throw new ArgumentNullException(nameof(action));
			}

			if (validate == null)
			{
				throw new ArgumentNullException(nameof(validate));
			}

			return new RelayCommand<T>(action, validate);
		}
	}


}

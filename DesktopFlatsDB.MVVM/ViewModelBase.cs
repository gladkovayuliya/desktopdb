﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DesktopFlatsDB.MVVM
{
	public abstract class ViewModelBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void UpdateValue<T>(T value, ref T storage,
			[CallerMemberName] string propertyName = default(string))
		{
			if (EqualityComparer<T>.Default.Equals(value, storage))
			{
				return;
			}

			storage = value;
			this.OnPropertyChanged(propertyName);
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = default(string))
		{
			var arguments = new PropertyChangedEventArgs(propertyName);
			PropertyChanged?.Invoke(this, arguments);
		}
	}
}

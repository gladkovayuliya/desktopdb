﻿using System;

namespace DesktopFlatsDB.MVVM
{
	internal sealed class RelayCommand<TArgument> : RelayCommandBase
	{
		public RelayCommand(Action<TArgument> execute)
		{
			_execute = execute ?? 
				throw new ArgumentNullException(nameof(execute));
		}
			
		public RelayCommand(Action<TArgument> execute, Func<TArgument, bool> validate)
			: this(execute)
		{
			_validate = validate ??
				throw new ArgumentNullException(nameof(validate));
		}

		public override bool CanExecute(object parameter) =>
			_validate == null || _validate((TArgument)parameter);

		public override void Execute(object parameter) =>
			_execute((TArgument)parameter);

		private readonly Action<TArgument> _execute;
		private readonly Func<TArgument, bool> _validate;
	}
}

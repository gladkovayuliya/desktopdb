﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace DesktopFlatsDB.MVVM
{
	public abstract class ValidatableViewModelBase : ViewModelBase, IDataErrorInfo
	{
		public bool IsValid =>
			Errors.Count == 0;

		public string this[string columnName] => 
			Errors.TryGetValue(columnName, out string val) 
				? val
				: default(String);

		public string Error => String.Join(Environment.NewLine, Errors.Values);

		protected object ValidationModel
		{
			get => _model;
			set => this.UpdateModel(value);
		}

		internal IDictionary<string, string> Errors { get; } = 
			new Dictionary<string, string>();

		internal ValidationContext SelfValidationContext { get; }

		internal ValidationContext ModelValidationContext { get; private set; }

		internal List<ValidationResult> ValidationResults { get; } =
			new List<ValidationResult>();

		protected ValidatableViewModelBase(object model)
		{
			ValidationModel = model;
			SelfValidationContext = this.CreateContext(this);
		}

		protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			this.ProcessValidation(propertyName);
		}

		protected virtual string ValidateProperty(string propertyName) => String.Empty;

		private void UpdateModel(object model)
		{
			_model = model ??
				throw new ArgumentNullException(nameof(model));

			ModelValidationContext = this.CreateContext(_model);
		}

		private void ProcessValidation(string propertyName)
		{
			Errors.Clear();

			this.TryAddValidationError(propertyName, 
				this.ValidateProperty(propertyName));
			this.Validate(ModelValidationContext, propertyName);
			this.Validate(SelfValidationContext, propertyName);

		}

		private void Validate (ValidationContext context, string defaultKey)
		{
			ValidationResults.Clear();

			if (Validator.TryValidateObject(context.ObjectInstance, context, ValidationResults, validateAllProperties: true))
			{
				return;
			}

			foreach(var result in ValidationResults)
			{
				var key = String.Concat(result.MemberNames);

				if (String.IsNullOrWhiteSpace(key))
				{
					key = defaultKey;
				}

				this.TryAddValidationError(key, result.ErrorMessage);
			}
		}

		private ValidationContext CreateContext(object target)
		{
			var context = new ValidationContext(target);
			this.Validate(context, String.Empty);

			return context;
		}

		private void TryAddValidationError(string key, string message)
		{
			if (String.IsNullOrEmpty(message))
			{
				Errors.Remove(key);
			}
			else
			{
				Errors[key] = message;
			}

		}

		private object _model;
	}


}

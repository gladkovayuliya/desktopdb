﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DesktopFlatsDB.UI.Converters
{
	public class BoolToStringConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is bool val)
			{
				return val
					? "Есть"
					: "Нет";
			}
			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}

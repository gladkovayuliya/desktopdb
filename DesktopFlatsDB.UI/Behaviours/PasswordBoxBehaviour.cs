﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using DesktopFlatsDB.Utility.Extensions;

namespace DesktopFlatsDB.UI.Behaviours
{
	public class PasswordBoxBehaviour: Behavior<PasswordBox>
	{
		public static readonly DependencyProperty InsecurePasswordProperty = DependencyProperty.Register("InsecurePassword",
			typeof(string), typeof(PasswordBoxBehaviour), new UIPropertyMetadata(null, OnInsecurePasswordChanged));

		public string InsecurePassword
		{
			get => (string)this.GetValue(InsecurePasswordProperty);
			set => this.SetValue(InsecurePasswordProperty, value);
		}

		protected override void OnAttached()
		{
			base.OnAttached();
			AssociatedObject.PasswordChanged += this.OnSecurePasswordChanged;
			AssociatedObject.Password = InsecurePassword;
		}

		private void OnSecurePasswordChanged(object sender, RoutedEventArgs e) =>
			this.InsecurePassword = AssociatedObject.SecurePassword.FromSecureToString();

		private static void OnInsecurePasswordChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (sender is PasswordBoxBehaviour self &&
				e.NewValue is string password &&
				!String.Equals(self.AssociatedObject.Password, password))
			{
				self.AssociatedObject.Password = password;
			}
		}
	}
}

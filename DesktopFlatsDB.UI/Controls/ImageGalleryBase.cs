﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace DesktopFlatsDB.UI.Controls
{
	public abstract class ImageGalleryBase: Control
	{
		public static readonly DependencyProperty ImagesProperty = DependencyProperty.Register("Images",
			typeof(ObservableCollection<BitmapImage>),
			typeof(ImageGalleryBase),
			new PropertyMetadata(new ObservableCollection<BitmapImage>(), ImagesCollectionChanged));

		public static readonly DependencyProperty CurrentProperty = DependencyProperty.Register("Current",
			typeof(BitmapImage),
			typeof(ImageGalleryBase),
			new PropertyMetadata(null));

		public ObservableCollection<BitmapImage> Images
		{
			get => (ObservableCollection<BitmapImage>)this.GetValue(ImagesProperty);
			set => this.SetValue(ImagesProperty, value);
		}

		public BitmapImage Current
		{
			get => (BitmapImage)this.GetValue(CurrentProperty);
			set => this.SetValue(CurrentProperty, value);
		}
		
		public ImageGalleryBase()
		{
			Images.CollectionChanged += this.ImagesChanged;
		}

		private void ImagesChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (sender is ImageGalleryBase gallery)
			{
				if (e.NewItems.Count > 0)
				{
					gallery.Current = (BitmapImage)e.NewItems[0];
				}
				else
				{
					gallery.Current = null;
				}
			}
		}


		private static void ImagesCollectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (d is ImageGalleryBase sender && e.NewValue is ObservableCollection<BitmapImage> newImages)
			{
				if (newImages.Count > 0)
				{
					sender.Current = newImages[0];
				}
				else
				{
					sender.Current = null;
				}
			}
		}
	}
}

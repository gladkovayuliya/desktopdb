﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace DesktopFlatsDB.UI.Controls
{
	public class ImageGallery: Control
	{
		public static readonly DependencyProperty ImagesProperty = DependencyProperty.Register("Images",
			typeof(ObservableCollection<BitmapSource>),
			typeof(ImageGallery),
			new PropertyMetadata(ImagesCollectionChanged));

		public static readonly DependencyProperty CurrentProperty = DependencyProperty.Register("Current",
			typeof(BitmapSource),
			typeof(ImageGallery),
			new PropertyMetadata(null));

		public static readonly DependencyProperty IsEditableProperty = DependencyProperty.Register("IsEditable",
			typeof(bool),
			typeof(ImageGallery),
			new PropertyMetadata(false));

		public ObservableCollection<BitmapSource> Images
		{
			get => (ObservableCollection<BitmapSource>)this.GetValue(ImagesProperty);
			set => this.SetValue(ImagesProperty, value);
		}

		public BitmapSource Current
		{
			get => (BitmapSource)this.GetValue(CurrentProperty);
			set => this.SetValue(CurrentProperty, value);
		}

		public bool IsEditable
		{
			get => (bool)this.GetValue(IsEditableProperty);
			set => this.SetValue(IsEditableProperty, value);
		}

		public double CurrentImageHeight =>
			Height * 0.6;

		public double ImagesListBoxHeight =>
			Height * 0.3;

		public double ImagesListBoxItemsHeight =>
			ImagesListBoxHeight - 50;

		public ImageGallery()
		{
			Loaded += this.OnLoaded;
			
		}


		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			var box = (ListBox) Template.FindName("ListBox", this);
			box.MouseDoubleClick += this.OnItemSelected;
		}

		private void OnItemSelected(object sender, MouseButtonEventArgs e)
		{
			if(!IsEditable)
			{
				return;
			}

			var currentIndex = Images.IndexOf(Current);
			Images.Remove(Current);

			if (Images.Count  > currentIndex)
			{
				Current = Images[currentIndex];
				return;
			}

			Current = Images.Count > 0
				? Images[currentIndex - 1]
				: null;
		}

		private void ImagesChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (sender is ImageGallery gallery)
			{
				if (e.NewItems.Count > 0)
				{
					gallery.Current = (BitmapSource)e.NewItems[0];
				}
				else
				{
					gallery.Current = null;
				}
			}
		}

		private static void ImagesCollectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (d is ImageGallery sender && e.NewValue is ObservableCollection<BitmapSource> newImages)
			{
				if (newImages.Count > 0)
				{
					sender.Current = newImages[0];
				}
				else
				{
					sender.Current = null;
				}
			}
		}
	}
}

﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace DesktopFlatsDB.UI.Controls
{
	public class SingleImageGallery: Control
	{
		public static readonly DependencyProperty ImagesProperty = DependencyProperty.Register("Images",
		typeof(ObservableCollection<BitmapSource>),
		typeof(SingleImageGallery),
		new PropertyMetadata(new ObservableCollection<BitmapSource>(), ImagesCollectionChanged));

		public static readonly DependencyProperty CurrentProperty = DependencyProperty.Register("Current",
			typeof(BitmapSource),
			typeof(SingleImageGallery),
			new PropertyMetadata(null));
		

		public ObservableCollection<BitmapSource> Images
		{
			get => (ObservableCollection<BitmapSource>)this.GetValue(ImagesProperty);
			set => this.SetValue(ImagesProperty, value);
		}

		public BitmapSource Current
		{
			get => (BitmapImage)this.GetValue(CurrentProperty);
			set => this.SetValue(CurrentProperty, value);
		}


		public double ImageWidth =>
			Width - 60;

		public double ImageHeight =>
			Height - 60;

		public SingleImageGallery()
		{
			Loaded += this.OnLoaded;
		}

		public int CurrentIndex { get; set; }

		public bool NoImages { get; set; }

		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			var leftButton = (Button)this.Template.FindName("LeftButton", this);
			var rightButton = (Button)this.Template.FindName("RightButton", this);
			leftButton.Click += this.OnLeftButtonClick;
			rightButton.Click += this.OnRightButtonClick;
		}

		private void OnRightButtonClick(object sender, RoutedEventArgs e)
		{
			if (NoImages)
			{
				return;
			}
			if (CurrentIndex + 1 == Images.Count)
			{
				return;
			}
			++CurrentIndex;
			Current = Images[CurrentIndex];
		}

		private void OnLeftButtonClick(object sender, RoutedEventArgs e)
		{
			if (NoImages)
			{
				return;
			}
			if (CurrentIndex == 0)
			{
				return;
			}
			--CurrentIndex;
			Current = Images[CurrentIndex];
		}

		private static void ImagesCollectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (d is SingleImageGallery sender && e.NewValue is ObservableCollection<BitmapSource> newImages)
			{
				if (newImages.Count > 0)
				{
					sender.Current = newImages[0];
					sender.CurrentIndex = 0;
					sender.NoImages = false;

				}
				else
				{
					var uri = new Uri("pack://application:,,,/DesktopFlatsDB.UI;component/Images/noimages.png");
					var image = new BitmapImage(uri);
					sender.Current = image;
					sender.NoImages = true;
					
				}
			}
		}



	}
}
